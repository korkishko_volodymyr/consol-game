package com.griddynamics.intern;

import com.griddynamics.intern.enums.SpecializationEnum;
import com.griddynamics.intern.factory.HeroInitializer;

import java.util.Arrays;

public class Launcher {

    public static void main(String[] args) {
        HeroInitializer heroInitializer = new HeroInitializer();
        heroInitializer.initializeHero();
    }
}
