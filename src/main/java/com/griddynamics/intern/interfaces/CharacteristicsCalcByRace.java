package com.griddynamics.intern.interfaces;

import com.griddynamics.intern.value.Characteristics;

/**
 * interface is intended to set behavior in the calculation of characteristics according to the selected race
 */

public interface CharacteristicsCalcByRace {
    Characteristics calculateHeroCharacteristic();
}
