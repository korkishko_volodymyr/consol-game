package com.griddynamics.intern.interfaces;

import com.griddynamics.intern.entity.RaceBonuses;

/**
 * interface is intended to set behavior in the calculation of bonuses according to the selected race
 */

public interface RaceBonusesInitializer {
    RaceBonuses initializeRaceBonuses();
}
