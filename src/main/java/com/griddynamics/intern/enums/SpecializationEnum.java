package com.griddynamics.intern.enums;

public enum SpecializationEnum {

    SWORDSMAN, ARCHER,

    SORCERER, ROGUE,

    WARRIOR
}
