package com.griddynamics.intern.scanner;

import com.griddynamics.intern.entity.Hero;
import com.griddynamics.intern.enums.RaceEnum;
import com.griddynamics.intern.enums.SpecializationEnum;
import com.griddynamics.intern.value.Characteristics;

import java.util.*;


public class ConsoleScanner {

    private static final String START_MESSAGE = "You have 30 Bonus points, distribute them among hero characteristics";
    private static final String QUESTION = "Do you confirm creating with this characteristic? yes/no: \n";
    private static final String CREATED_MESSAGE = "Hero created!";
    private static final String NO_POINTS = "No points available, try again";
    private static final String NOT_NUMBER = "Should be a number!";
    private static final String AGILITY = "agility";
    private static final String WILLPOWER = "willpower";
    private static final String DEXTERITY = "dexterity";
    private static final String STRENGTH = "strength";
    private static final String INTELLECT = "intellect";
    private static final String CHARISMA = "charisma";
    private static final String STAMINA = "stamina";
    private static double points = Hero.getSkillPoints();

    private List<String> characteristicList = Arrays.asList(WILLPOWER, AGILITY, DEXTERITY, STRENGTH,
            INTELLECT, CHARISMA, STAMINA);

    private Map<String, Integer> characteristicMap = new HashMap<>();

    private final Scanner scanner = new Scanner(System.in);

    public String getHeroName() {
        System.out.print("InputHeroName:");
        return scanner.nextLine();
    }

    public int getHeroRace() {
        System.out.println("Choose Hero Race \n" + Arrays.asList(RaceEnum.values()));
        while (!scanner.hasNext("[1234]")) {
            System.out.println("NO THIS RACE");
            scanner.next();
        }
        isInteger();
        return scanner.nextInt();
    }

    public int getHeroSpecialization(List<SpecializationEnum> availableSpecializations) {
        System.out.println("Choose Hero Specialization\n" + availableSpecializations);
        isInteger();
        return scanner.nextInt();
    }

    public Characteristics modifyHeroCharacteristics(Map<String, Integer> raceCoefMap, Characteristics characteristics) {
        System.out.println(START_MESSAGE);
        for (String charName : characteristicList) {
            System.out.println("One point of " + charName + " cost " + raceCoefMap.get(charName));
            System.out.print("Input " + charName + " value: ");
            scanCharacteristicValue(charName, raceCoefMap, characteristics);
        }
        System.out.print(QUESTION);
        System.out.println(fillCharacteristics(characteristics));
        if (scanner.next().equals("yes")) {
            System.out.println(CREATED_MESSAGE);
        } else {
            points = 30;
            modifyHeroCharacteristics(raceCoefMap, characteristics);
        }
        return fillCharacteristics(characteristics);
    }

    private void scanCharacteristicValue(String characteristicName, Map<String, Integer> raceCoefMap,
                                         Characteristics characteristics) {
        isInteger();
        int inputtedValue = scanner.nextInt();
        double lostPoints = calculateResultPoints(inputtedValue, raceCoefMap.get(characteristicName));
        if (lostPoints < 0) {
            System.out.println(NO_POINTS);
            points = 30;
            modifyHeroCharacteristics(raceCoefMap, characteristics);
        } else {
            System.out.println("Now you have " + lostPoints + " bonus points");
            characteristicMap.put(characteristicName, inputtedValue);
        }
    }

    private double calculateResultPoints(int temp, int raceCoef) {
        points -= temp * raceCoef;
        return points;
    }

    private Characteristics fillCharacteristics(Characteristics characteristics) {
        return Characteristics.builder()
                .willPower(characteristics.getWillPower() + characteristicMap.get(WILLPOWER))
                .strength(characteristics.getStrength() + characteristicMap.get(STRENGTH))
                .stamina(characteristics.getStamina() + characteristicMap.get(STAMINA))
                .intellect(characteristics.getIntellect() + characteristicMap.get(INTELLECT))
                .agility(characteristics.getAgility() + characteristicMap.get(AGILITY))
                .charisma(characteristics.getCharisma() + characteristicMap.get(CHARISMA))
                .dexterity(characteristics.getDexterity() + characteristicMap.get(DEXTERITY))
                .build();
    }

    private void isInteger() {
        while (!scanner.hasNextInt()) {
            System.out.println(NOT_NUMBER);
            System.out.print("Input correct value: ");
            scanner.next();
        }
    }
}

