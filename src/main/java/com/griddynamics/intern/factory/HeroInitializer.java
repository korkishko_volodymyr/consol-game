package com.griddynamics.intern.factory;

import com.griddynamics.intern.entity.Race;
import com.griddynamics.intern.entity.item.Accessory;
import com.griddynamics.intern.entity.item.OneHandWeapon;
import com.griddynamics.intern.entity.item.accessory.head.Mask;
import com.griddynamics.intern.entity.item.onehand.Sword;
import com.griddynamics.intern.enums.RaceEnum;
import com.griddynamics.intern.enums.SpecializationEnum;
import com.griddynamics.intern.scanner.ConsoleScanner;
import com.griddynamics.intern.entity.Hero;
import com.griddynamics.intern.service.*;
import com.griddynamics.intern.value.Attributes;
import com.griddynamics.intern.value.Characteristics;

import java.util.Map;

/**
 * userRaceChoiceMap - conversion of entered number of race to the raceEnum value
 * userRaceChoiceMap - conversion of entered number of spec. to the specEnum value
 */

public class HeroInitializer {

    private AvailableSpecializations availableSpecializations = new AvailableSpecializations();
    private Hero myHero;
    private Map<Integer, RaceEnum> userRaceChoiceMap;
    private Map<Integer, SpecializationEnum> userSpecChoiceMap;

    public HeroInitializer() {
        initMaps();
    }

    private void initMaps() {
        userRaceChoiceMap = Map.of(
                1, RaceEnum.HUMAN,
                2, RaceEnum.ORC,
                3, RaceEnum.ELF,
                4, RaceEnum.DWARF);

        userSpecChoiceMap = Map.of(
                1, SpecializationEnum.SWORDSMAN,
                2, SpecializationEnum.ARCHER,
                3, SpecializationEnum.SORCERER,
                4, SpecializationEnum.ROGUE,
                5, SpecializationEnum.WARRIOR);
    }

    private ConsoleScanner consoleScanner = new ConsoleScanner();
    private CharacteristicConfigurerBySpecialization characteristicConfigurerBySpecialization =
            new CharacteristicConfigurerBySpecialization();
    private CharacteristicInitializerByRace characteristicInitializer =
            new CharacteristicInitializerByRace();
    private BonusesInitializer bonusesInitializer = new BonusesInitializer();

    public void initializeHero() {
        String heroName = initializeHeroName();
        RaceEnum raceChoice = initializeHeroRace();
        Race selectedRace = buildRace(raceChoice);
        SpecializationEnum specChoice = initializeHeroSpecialization(raceChoice);
        Characteristics characteristics = characteristicInitializer.initializeHeroCharacteristic(raceChoice);
        characteristics = characteristicConfigurerBySpecialization.
                initializeHeroCharacteristicBySpecialization(specChoice, characteristics);
        Attributes heroAttributes = AttributesConfigurer.calculateAttributes(characteristics);
        characteristics = initializeHeroCharacteristics(selectedRace, characteristics);
        myHero = buildHero(heroName, heroAttributes, characteristics, selectedRace, specChoice);
        System.out.println(myHero);
        OneHandWeapon weapon = new Sword();
        Accessory accessory = new Mask();
        myHero.equip(weapon);
        myHero.equip(accessory);
        // hero is created
    }

    private String initializeHeroName() {
        return consoleScanner.getHeroName();
    }

    private Characteristics initializeHeroCharacteristics(Race selectedRace, Characteristics characteristics) {
        return consoleScanner.modifyHeroCharacteristics(bonusesInitializer.
                initializeRaceCoefMap(selectedRace), characteristics);
    }

    private RaceEnum initializeHeroRace() {
        int heroRace = consoleScanner.getHeroRace();
        return userRaceChoiceMap.get(heroRace);
    }

    private SpecializationEnum initializeHeroSpecialization(RaceEnum raceChoice) {
        int heroSpecialization = consoleScanner.getHeroSpecialization(availableSpecializations
                .getAvailableSpecializations(raceChoice));
        return userSpecChoiceMap.get(heroSpecialization);
    }
    //build Race with raceBonuses according to the selected race

    private Race buildRace(RaceEnum raceEnum) {
        return Race.builder()
                .raceBonuses(bonusesInitializer.getBonuses(raceEnum))
                .raceEnum(raceEnum)
                .build();
    }

    private Hero buildHero(String name, Attributes attributes,
                           Characteristics characteristics,
                           Race race, SpecializationEnum specialization) {
        return Hero.builder()
                .attributes(attributes)
                .characteristics(characteristics)
                .name(name)
                .race(race)
                .specialization(specialization)
                .armor(0)
                .build();
    }


}


