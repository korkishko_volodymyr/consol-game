package com.griddynamics.intern.value;

import lombok.*;

@Builder(toBuilder = true)
@AllArgsConstructor
@Data
public class Attributes {

    private double health;
    private double mana;
    private double rage;

    @Override
    public String toString() {
        return "health=" + health +
                ", mana=" + mana +
                ", rage=" + rage;
    }
}
