package com.griddynamics.intern.value;

import lombok.*;

@Builder(toBuilder = true)
@Data
public class Characteristics {

    private int willPower;
    private int agility;
    private int dexterity;
    private int charisma;
    private int stamina;
    private int strength;
    private int intellect;

    @Override
    public String toString() {
        return "willPower=" + willPower +
                ", agility=" + agility +
                ", dexterity=" + dexterity +
                ", charisma=" + charisma +
                ", stamina=" + stamina +
                ", strength=" + strength +
                ", intellect=" + intellect;
    }
}
