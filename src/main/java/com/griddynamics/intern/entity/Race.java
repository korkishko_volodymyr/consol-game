package com.griddynamics.intern.entity;

import com.griddynamics.intern.enums.RaceEnum;
import lombok.*;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
public class Race {

    private RaceEnum raceEnum;
    private RaceBonuses raceBonuses;

    @Override
    public String toString() {
        return raceEnum.toString();
    }
}
