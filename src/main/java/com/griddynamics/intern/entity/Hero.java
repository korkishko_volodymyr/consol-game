package com.griddynamics.intern.entity;

import com.griddynamics.intern.entity.item.Accessory;
import com.griddynamics.intern.entity.item.OneHandWeapon;
import com.griddynamics.intern.entity.item.TwoHandWeapon;
import com.griddynamics.intern.enums.SpecializationEnum;
import com.griddynamics.intern.value.Attributes;
import com.griddynamics.intern.value.Characteristics;
import lombok.*;

@Builder(toBuilder = true)
@Data
public class Hero {

    private String name;
    private Race race;
    private SpecializationEnum specialization;
    private Characteristics characteristics;
    private OneHandWeapon oneHandWeapon;
    private Attributes attributes;
    private Accessory accessory;
    private Level level;
    private static double points = 30;
    private int armor;

    public static double getSkillPoints() {
        return points;
    }

    public void equip(OneHandWeapon oneHandWeapon) {
        if (oneHandWeapon.getAvailableSpecializations().contains(specialization)) {
            System.out.println("EQUIPPED");
        } else {
            System.out.println("HE DOESN`T");
        }
    }

    public void equip(TwoHandWeapon twoHandWeapon) {
        if (twoHandWeapon.getAvailableSpecializations().contains(specialization)) {
            System.out.println("EQUIPPED");
        } else {
            System.out.println("HE DOESNT");
        }
    }

    public void equip(Accessory accessory) {
        if (accessory.getAvailableSpecializations().contains(specialization)) {
            System.out.println("EQUIPPED");
        } else {
            System.out.println("NONONO");
        }
    }

    @Override
    public String toString() {
        return "Hero: " +
                "\nname = " + name + '\n' +
                "race = " + race + '\n' +
                "specialization = " + specialization + '\n' +
                "characteristics = " + characteristics + '\n' +
                "attributes = " + attributes + '\n' +
                "armor = " + armor;
    }

}
