package com.griddynamics.intern.entity.item.accessory.body;

import com.griddynamics.intern.entity.item.Accessory;
import com.griddynamics.intern.enums.SpecializationEnum;
import com.griddynamics.intern.value.Attributes;

import java.util.Collections;
import java.util.List;

public class Robe implements Accessory {
    @Override
    public List<SpecializationEnum> getAvailableSpecializations() {
        return Collections.singletonList(SpecializationEnum.SORCERER);
    }

    @Override
    public Attributes upgradeAttributes() {
        return null;
    }
}
