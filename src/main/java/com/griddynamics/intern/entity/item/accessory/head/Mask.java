package com.griddynamics.intern.entity.item.accessory.head;

import com.griddynamics.intern.entity.item.Accessory;
import com.griddynamics.intern.enums.SpecializationEnum;
import com.griddynamics.intern.value.Attributes;

import java.util.Collections;
import java.util.List;

public class Mask implements Accessory {
    @Override
    public List<SpecializationEnum> getAvailableSpecializations() {
        return Collections.singletonList(SpecializationEnum.ROGUE);
    }

    @Override
    public Attributes upgradeAttributes() {
        return null;
    }
}
