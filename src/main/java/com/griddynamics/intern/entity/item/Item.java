package com.griddynamics.intern.entity.item;

import com.griddynamics.intern.enums.SpecializationEnum;
import com.griddynamics.intern.value.Attributes;

import java.util.List;

public interface Item {
    List<SpecializationEnum> getAvailableSpecializations();

    Attributes upgradeAttributes();
}
