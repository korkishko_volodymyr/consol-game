package com.griddynamics.intern.entity.item.twohand;

import com.griddynamics.intern.entity.item.TwoHandWeapon;
import com.griddynamics.intern.enums.SpecializationEnum;
import com.griddynamics.intern.value.Attributes;

import java.util.Collections;
import java.util.List;

public class Bow implements TwoHandWeapon {
    @Override
    public List<SpecializationEnum> getAvailableSpecializations() {
        return Collections.singletonList(SpecializationEnum.ARCHER);
    }

    @Override
    public Attributes upgradeAttributes() {
        return null;
    }
}
