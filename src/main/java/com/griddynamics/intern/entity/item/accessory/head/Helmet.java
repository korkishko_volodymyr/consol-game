package com.griddynamics.intern.entity.item.accessory.head;

import com.griddynamics.intern.entity.item.Accessory;
import com.griddynamics.intern.enums.SpecializationEnum;
import com.griddynamics.intern.value.Attributes;

import java.util.Arrays;
import java.util.List;

public class Helmet implements Accessory {
    @Override
    public List<SpecializationEnum> getAvailableSpecializations() {
        return Arrays.asList(SpecializationEnum.SWORDSMAN,SpecializationEnum.WARRIOR);
    }

    @Override
    public Attributes upgradeAttributes() {
        return null;
    }
}
