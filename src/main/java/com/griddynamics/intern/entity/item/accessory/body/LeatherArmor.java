package com.griddynamics.intern.entity.item.accessory.body;

import com.griddynamics.intern.entity.item.Accessory;
import com.griddynamics.intern.enums.SpecializationEnum;
import com.griddynamics.intern.value.Attributes;

import java.util.Arrays;
import java.util.List;

public class LeatherArmor implements Accessory {
    @Override
    public List<SpecializationEnum> getAvailableSpecializations() {
        return Arrays.asList(SpecializationEnum.ROGUE,SpecializationEnum.ARCHER);
    }

    @Override
    public Attributes upgradeAttributes() {
        return null;
    }
}
