package com.griddynamics.intern.entity.item.onehand;

import com.griddynamics.intern.entity.item.OneHandWeapon;
import com.griddynamics.intern.enums.SpecializationEnum;
import com.griddynamics.intern.value.Attributes;

import java.util.Collections;
import java.util.List;

public class Shield implements OneHandWeapon {

    @Override
    public List<SpecializationEnum> getAvailableSpecializations() {
        return Collections.singletonList(SpecializationEnum.SWORDSMAN);
    }

    @Override
    public Attributes upgradeAttributes() {
        return null;
    }
}
