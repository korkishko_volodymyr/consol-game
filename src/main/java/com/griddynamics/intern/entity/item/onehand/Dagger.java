package com.griddynamics.intern.entity.item.onehand;

import com.griddynamics.intern.entity.item.OneHandWeapon;
import com.griddynamics.intern.enums.SpecializationEnum;
import com.griddynamics.intern.value.Attributes;

import java.util.Arrays;
import java.util.List;

public class Dagger implements OneHandWeapon {

    @Override
    public List<SpecializationEnum> getAvailableSpecializations() {
        return Arrays.asList(SpecializationEnum.ARCHER,SpecializationEnum.ROGUE);
    }

    @Override
    public Attributes upgradeAttributes() {
        return null;
    }
}
