package com.griddynamics.intern.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder(toBuilder = true)
public class RaceBonuses {

    private int willPowerBonus;
    private int agilityBonus;
    private int dexterityBonus;
    private int charismaBonus;
    private int staminaBonus;
    private int strengthBonus;
    private int intellectBonus;

}
