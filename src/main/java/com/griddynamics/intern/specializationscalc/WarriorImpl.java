package com.griddynamics.intern.specializationscalc;

import com.griddynamics.intern.interfaces.CharacteristicsCalcBySpecialization;
import com.griddynamics.intern.value.Characteristics;

public class WarriorImpl implements CharacteristicsCalcBySpecialization {
    private static final int AGILITY_BONUS = 1;
    private static final int DEXTERITY_BONUS = 1;
    private static final int STRENGTH_BONUS = 2;

    @Override
    public Characteristics calculateCharacteristicBySpecialization(Characteristics characteristics) {
        return Characteristics.builder()
                .agility(characteristics.getAgility() - AGILITY_BONUS)
                .charisma(characteristics.getCharisma())
                .dexterity(characteristics.getDexterity() - DEXTERITY_BONUS)
                .intellect(characteristics.getIntellect())
                .stamina(characteristics.getStamina())
                .strength(characteristics.getStrength() + STRENGTH_BONUS)
                .willPower(characteristics.getWillPower())
                .build();
    }
}

