package com.griddynamics.intern.specializationscalc;

import com.griddynamics.intern.interfaces.CharacteristicsCalcBySpecialization;
import com.griddynamics.intern.value.Characteristics;

public class SorcererImpl implements CharacteristicsCalcBySpecialization {
    private static final int AGILITY_BONUS = 2;
    private static final int INTELLECT_BONUS = 1;
    private static final int WILLPOWER_BONUS = 1;

    @Override
    public Characteristics calculateCharacteristicBySpecialization(Characteristics characteristics) {
        return Characteristics.builder()
                .agility(characteristics.getAgility() - AGILITY_BONUS)
                .charisma(characteristics.getCharisma())
                .dexterity(characteristics.getDexterity())
                .intellect(characteristics.getIntellect() + INTELLECT_BONUS)
                .stamina(characteristics.getStamina())
                .strength(characteristics.getStrength())
                .willPower(characteristics.getWillPower() + WILLPOWER_BONUS)
                .build();
    }
}
