package com.griddynamics.intern.specializationscalc;

import com.griddynamics.intern.interfaces.CharacteristicsCalcBySpecialization;
import com.griddynamics.intern.value.Characteristics;

/**
 * implementation of the imposition of fines according to the chosen specialization
 */

public class RogueImpl implements CharacteristicsCalcBySpecialization {
    private static final int DEXTERITY_BONUS = 2;
    private static final int STRENGHT_BONUS = 1;
    private static final int WILLPOWER_BONUS = 1;

    @Override
    public Characteristics calculateCharacteristicBySpecialization(Characteristics characteristics) {
        return Characteristics.builder()
                .agility(characteristics.getAgility())
                .charisma(characteristics.getCharisma())
                .dexterity(characteristics.getDexterity() + DEXTERITY_BONUS)
                .intellect(characteristics.getIntellect())
                .stamina(characteristics.getStamina())
                .strength(characteristics.getStrength() - STRENGHT_BONUS)
                .willPower(characteristics.getWillPower() - WILLPOWER_BONUS)
                .build();
    }
}
