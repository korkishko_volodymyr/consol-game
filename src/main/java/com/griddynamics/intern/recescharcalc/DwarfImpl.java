package com.griddynamics.intern.recescharcalc;

import com.griddynamics.intern.interfaces.CharacteristicsCalcByRace;
import com.griddynamics.intern.value.Characteristics;

/**
 * implementation of the imposition of fines according to the chosen race
 */

public class DwarfImpl implements CharacteristicsCalcByRace {
    private static final int BASE_POINTS = 10;
    private static final int AGILITY_BONUS = 4;
    private static final int CHARISMA_BONUS = 2;
    private static final int DEXTERITY_BONUS = 2;
    private static final int INTELLECT_BONUS = 4;
    private static final int STAMINA_BONUS = 3;
    private static final int STRENGHT_BONUS = 3;
    private static final int WILLPOWER_BONUS = 2;

    @Override
    public Characteristics calculateHeroCharacteristic() {
        return Characteristics.builder()
                .agility(BASE_POINTS - AGILITY_BONUS)
                .charisma(BASE_POINTS - CHARISMA_BONUS)
                .dexterity(BASE_POINTS + DEXTERITY_BONUS)
                .intellect(BASE_POINTS - INTELLECT_BONUS)
                .stamina(BASE_POINTS + STAMINA_BONUS)
                .strength(BASE_POINTS + STRENGHT_BONUS)
                .willPower(BASE_POINTS + WILLPOWER_BONUS)
                .build();
    }
}
