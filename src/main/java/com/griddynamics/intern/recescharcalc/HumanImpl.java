package com.griddynamics.intern.recescharcalc;

import com.griddynamics.intern.interfaces.CharacteristicsCalcByRace;
import com.griddynamics.intern.value.Characteristics;

/**
 * implementation of the imposition of fines according to the chosen race
 */

public class HumanImpl implements CharacteristicsCalcByRace {
    private static final int BASE_POINTS = 10;

    @Override
    public Characteristics calculateHeroCharacteristic() {
        return Characteristics.builder()
                .willPower(BASE_POINTS)
                .strength(BASE_POINTS)
                .stamina(BASE_POINTS)
                .intellect(BASE_POINTS)
                .dexterity(BASE_POINTS)
                .charisma(BASE_POINTS)
                .agility(BASE_POINTS)
                .build();
    }
}
