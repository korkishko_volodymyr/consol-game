package com.griddynamics.intern.service;

import com.griddynamics.intern.value.Attributes;
import com.griddynamics.intern.value.Characteristics;

/**
 * implementation of the calculation of attributes
 */

public class AttributesConfigurer {

    private static final double HP_COEF = 0.65;
    private static final double MANA_COEF = 0.4;
    private static final double RAGE_COEF = 0.25;
    private static final double HP_REGEN = 0.25;
    private static final double MANA_REGEN = 0.15;
    private static final double RAGE_REGEN = 0.15;

    public static Attributes calculateAttributes(Characteristics characteristic) {
        double health = characteristic.getStrength() * (HP_COEF * (characteristic.getStamina()
                + (double) (characteristic.getAgility() / characteristic.getStrength())));
        double mana = characteristic.getWillPower() * (MANA_COEF * ((characteristic.getCharisma()
                + (double) characteristic.getIntellect()) / characteristic.getWillPower()));
        double rage = characteristic.getStrength() * RAGE_COEF * ((characteristic.getStamina()
                - (double) characteristic.getIntellect()) / characteristic.getStrength());
        rage = rage >= 0 ? rage : 0;
        return Attributes.builder()
                .health(health)
                .mana(mana)
                .rage(rage)
                .build();
    }

    public static Attributes regenerateAttributes(Characteristics characteristics, Attributes attributes) {
        double regenHp = attributes.getHealth() + (4 * characteristics.getStrength()
                + 3 * characteristics.getStamina()
                + characteristics.getAgility()) * HP_REGEN;

        double regenMana = attributes.getMana() + (3 * characteristics.getWillPower()
                + 3 * characteristics.getIntellect()) * MANA_REGEN;

        double regenRage = attributes.getRage() + (2 * characteristics.getStrength()
                + 2 * characteristics.getStamina()
                + 2 * characteristics.getIntellect()) * RAGE_REGEN;
        return Attributes.builder()
                .health(regenHp)
                .mana(regenMana)
                .rage(regenRage)
                .build();
    }
}
