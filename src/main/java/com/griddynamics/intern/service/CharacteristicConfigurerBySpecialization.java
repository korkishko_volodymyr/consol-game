package com.griddynamics.intern.service;

import com.griddynamics.intern.enums.SpecializationEnum;
import com.griddynamics.intern.interfaces.CharacteristicsCalcBySpecialization;
import com.griddynamics.intern.specializationscalc.*;
import com.griddynamics.intern.value.Characteristics;

import java.util.EnumMap;
import java.util.Map;

public class CharacteristicConfigurerBySpecialization {

    private Map<SpecializationEnum, CharacteristicsCalcBySpecialization> raceEnumMap = new EnumMap<>(SpecializationEnum.class) {{
        put(SpecializationEnum.SWORDSMAN, (characteristics) -> new SwordsmanImpl().
                calculateCharacteristicBySpecialization(characteristics));
        put(SpecializationEnum.ARCHER, (characteristics) -> new ArcherImpl().
                calculateCharacteristicBySpecialization(characteristics));
        put(SpecializationEnum.SORCERER, (characteristics) -> new SorcererImpl().
                calculateCharacteristicBySpecialization(characteristics));
        put(SpecializationEnum.ROGUE, (characteristics) -> new RogueImpl().
                calculateCharacteristicBySpecialization(characteristics));
        put(SpecializationEnum.WARRIOR, (characteristics) -> new WarriorImpl().
                calculateCharacteristicBySpecialization(characteristics));
    }};

    // method returns hero characteristics according to the specialization
    public Characteristics initializeHeroCharacteristicBySpecialization(SpecializationEnum specialization,
                                                                        Characteristics characteristics) {
        return raceEnumMap.get(specialization)
                .calculateCharacteristicBySpecialization(characteristics);
    }
}
