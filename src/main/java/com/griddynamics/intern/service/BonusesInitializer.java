package com.griddynamics.intern.service;

import com.griddynamics.intern.entity.Race;
import com.griddynamics.intern.entity.RaceBonuses;
import com.griddynamics.intern.enums.RaceEnum;
import com.griddynamics.intern.interfaces.RaceBonusesInitializer;

import java.util.HashMap;
import java.util.Map;

/**
 * implementation of fines imposed on user-entered bonuses to features
 */

public class BonusesInitializer {

    private Map<RaceEnum, RaceBonusesInitializer> bonusesInitializerMap = new HashMap<>() {{
        put(RaceEnum.HUMAN, () -> initializeHumanBonuses());
        put(RaceEnum.ORC, () -> initializeOrcBonuses());
        put(RaceEnum.ELF, () -> initializeElfBonuses());
        put(RaceEnum.DWARF, () -> initializeDwarfBonuses());

    }};

    private RaceBonuses initializeHumanBonuses() {
        return RaceBonuses.builder()
                .agilityBonus(3)
                .willPowerBonus(2)
                .dexterityBonus(3)
                .strengthBonus(4)
                .intellectBonus(3)
                .charismaBonus(4)
                .staminaBonus(2)
                .build();
    }

    private RaceBonuses initializeOrcBonuses() {
        return RaceBonuses.builder()
                .agilityBonus(4)
                .willPowerBonus(4)
                .dexterityBonus(4)
                .strengthBonus(2)
                .intellectBonus(4)
                .charismaBonus(8)
                .staminaBonus(3)
                .build();
    }

    private RaceBonuses initializeElfBonuses() {
        return RaceBonuses.builder()
                .agilityBonus(1)
                .willPowerBonus(2)
                .dexterityBonus(2)
                .strengthBonus(6)
                .intellectBonus(2)
                .charismaBonus(2)
                .staminaBonus(6)
                .build();
    }

    private RaceBonuses initializeDwarfBonuses() {
        return RaceBonuses.builder()
                .agilityBonus(4)
                .willPowerBonus(1)
                .dexterityBonus(2)
                .strengthBonus(3)
                .intellectBonus(4)
                .charismaBonus(8)
                .staminaBonus(2)
                .build();
    }

    public RaceBonuses getBonuses(RaceEnum race) {
        return bonusesInitializerMap.get(race).initializeRaceBonuses();
    }

    //initialize map with bonuses for characteristics according to selected race

    public Map<String, Integer> initializeRaceCoefMap(Race race) {
        return new HashMap<>() {{
            put("willpower", race.getRaceBonuses().getWillPowerBonus());
            put("agility", race.getRaceBonuses().getAgilityBonus());
            put("dexterity", race.getRaceBonuses().getDexterityBonus());
            put("charisma", race.getRaceBonuses().getCharismaBonus());
            put("stamina", race.getRaceBonuses().getStaminaBonus());
            put("strength", race.getRaceBonuses().getStrengthBonus());
            put("intellect", race.getRaceBonuses().getIntellectBonus());
        }};
    }
}
