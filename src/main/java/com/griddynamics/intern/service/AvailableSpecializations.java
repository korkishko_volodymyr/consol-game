package com.griddynamics.intern.service;

import com.griddynamics.intern.enums.RaceEnum;
import com.griddynamics.intern.enums.SpecializationEnum;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class AvailableSpecializations {

    private Map<RaceEnum, List<SpecializationEnum>> availableSpecializationsMap = Map.of(
            RaceEnum.HUMAN, Arrays.asList(SpecializationEnum.SWORDSMAN,
                    SpecializationEnum.ARCHER,
                    SpecializationEnum.SORCERER,
                    SpecializationEnum.ROGUE),
            RaceEnum.ORC, Arrays.asList(SpecializationEnum.SWORDSMAN, SpecializationEnum.WARRIOR),
            RaceEnum.DWARF, Arrays.asList(SpecializationEnum.SWORDSMAN, SpecializationEnum.WARRIOR),
            RaceEnum.ELF, Arrays.asList(SpecializationEnum.ARCHER, SpecializationEnum.SORCERER, SpecializationEnum.ROGUE));

    public List<SpecializationEnum> getAvailableSpecializations(RaceEnum race) {
        return availableSpecializationsMap.get(race);
    }
}
