package com.griddynamics.intern.service;

import com.griddynamics.intern.enums.RaceEnum;
import com.griddynamics.intern.recescharcalc.DwarfImpl;
import com.griddynamics.intern.recescharcalc.HumanImpl;
import com.griddynamics.intern.recescharcalc.OrcImpl;
import com.griddynamics.intern.recescharcalc.ElfImpl;
import com.griddynamics.intern.interfaces.CharacteristicsCalcByRace;
import com.griddynamics.intern.value.Characteristics;

import java.util.EnumMap;
import java.util.Map;

/**
 * implementation of initializing characteristics according to the race
 */

public class CharacteristicInitializerByRace {

    private Map<RaceEnum, CharacteristicsCalcByRace> raceEnumMap = new EnumMap<>(RaceEnum.class) {{
        put(RaceEnum.HUMAN, () -> new HumanImpl().calculateHeroCharacteristic());
        put(RaceEnum.DWARF, () -> new DwarfImpl().calculateHeroCharacteristic());
        put(RaceEnum.ELF, () -> new ElfImpl().calculateHeroCharacteristic());
        put(RaceEnum.ORC, () -> new OrcImpl().calculateHeroCharacteristic());
    }};

    public Characteristics initializeHeroCharacteristic(RaceEnum race) {
        return raceEnumMap.get(race).calculateHeroCharacteristic();
    }
}
